<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m181016_183433_user
 */
class m181016_183433_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->unique(),
            'fio' => $this->string(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $newUser = new User();
        $newUser->login = 'admin';
        $newUser->fio = 'admin';
        $newUser->generateAuthKey();
        $newUser->setPassword('admin');
        $newUser->save();

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
