<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList([null => 'Выбрать'] + \app\models\User::getList()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'date_begin')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Дата начала'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);?>
    <?= $form->field($model, 'date_end')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Дата сдачи'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
