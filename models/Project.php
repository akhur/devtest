<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $price
 * @property string $date_begin
 * @property string $date_end
 * @property int $created_at
 * @property int $updated_at
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'price', 'created_at', 'updated_at'], 'integer'],
            [['date_begin', 'date_end'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Название',
            'price' => 'Цена',
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата сдачи',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->getId();
        }
        return parent::beforeValidate();
    }

    /**
     * @return User
     */
    public function getLocation()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
